#include "AGraph.hpp"

AGraph::AGraph()
    : chart(new QChart)
    , axisY(new QValueAxis)
{
    chart->legend()->setVisible(false);

    axisY->setLabelFormat("%.00f");
    axisY->setTitleText("Custom Data");
    chart->addAxis(axisY, Qt::AlignLeft);
}

AGraph::~AGraph()
{}

qreal AGraph::getAverage(QVector<qreal> vec) {
    qreal sum= 0;

    for (QVector<qreal>::size_type i = 0; i < vec.size(); ++i)
        sum += vec[i];
    return (sum / vec.size());
}

QVector<QPair<QDateTime, qreal>> AGraph::getDataWithStep(QVector<QPair<QDateTime, qreal>> data, e_step step)
{
    QVector<QPair<QDateTime, qreal>> ret;
    QDateTime prev = data[0].first;
    QDateTime current;
    QVector<qreal> value_vec;
    unsigned int interval;

    if (step == HOUR)
        interval = 3600;
    else if (step == HOURS)
        interval = 10800;
    else
        interval = 86400;

    for (int i = 0; i < data.size(); ++i)
    {
        current = data[i].first;
        if (QDateTime(QDate(prev.date()), QTime(prev.time().hour(), 0, 0)).secsTo(QDateTime(QDate(current.date()), QTime(current.time().hour(), 0, 0))) >= interval)
        {
            ret.push_back(QPair<QDateTime, qreal>(current, getAverage(value_vec)));
            value_vec.clear();
            prev = current;
            value_vec.push_back(data[i].second);
        }
        else
            value_vec.push_back(data[i].second);
    }
    return ret;
}

QVector<QPair<QDateTime, qreal>> AGraph::getDataWithMaxVal(QVector<QPair<QDateTime, qreal>> data)
{
    QVector<QPair<QDateTime, qreal>> ret;
    qreal max;
    QDateTime prev = data[0].first;
    QDateTime current;
    bool is_first = true;
    unsigned int interval = 86400;

    for (int i = 0; i < data.size(); ++i)
    {
        current = data[i].first;
        if (is_first) {
            max = data[i].second;
            is_first = false;
            continue;
        }
        if (prev.secsTo(current) >= interval)
        {
            ret.push_back(QPair<QDateTime, qreal>(prev.date().startOfDay(), max));
            prev = current;
            is_first = true;
        }
        else
            max = (data[i].second > max) ? (data[i].second) : (max);
    }
    return ret;
}

QVector<QPair<QDateTime, qreal>> AGraph::getDataWithMinVal(QVector<QPair<QDateTime, qreal>> data)
{
    QVector<QPair<QDateTime, qreal>> ret;
    qreal min;
    QDateTime prev = data[0].first;
    QDateTime current;
    bool is_first = true;
    unsigned int interval = 86400;

    for (int i = 0; i < data.size(); ++i)
    {
        current = data[i].first;
        if (is_first) {
            min = data[i].second;
            is_first = false;
            continue;
        }
        if (prev.secsTo(current) >= interval)
        {
            ret.push_back(QPair<QDateTime, qreal>(prev.date().startOfDay(), min));
            prev = current;
            is_first = true;
        }
        else
            min = (data[i].second < min) ? (data[i].second) : (min);
    }
    return ret;
}

void AGraph::buildGraph(QDateTime from, QDateTime to,
                        QVector<QPair<QDateTime, qreal>> data,
                        e_averaging averaging) {
    if (averaging == MAX)
        exposeCharts(from, to, getDataWithMaxVal(data));
    else if (averaging == MIN)
        exposeCharts(from, to, getDataWithMinVal(data));
    else if (from.daysTo(to) >= 7)
        exposeCharts(from, to, getDataWithStep(data, DAY));
    else if (from.daysTo(to) >= 3)
        exposeCharts(from, to, getDataWithStep(data, HOURS));
    else if (from.daysTo(to) >= 1)
        exposeCharts(from, to, getDataWithStep(data, HOUR));
    else
        exposeCharts(from, to, data);
}
