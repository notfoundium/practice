#ifndef SENSOR_HPP
# define SENSOR_HPP

#  include <QString>
#  include <QVector>
#  include <QPair>
#  include <QDateTime>

class Sensor {
    public:
        Sensor(QString const& name);
        ~Sensor();

        void addData(QDateTime qdtime, qreal value);

        QString const& getName() const;
        QVector<QPair<QDateTime, qreal>> const& getData() const;

    private:
        QString name;

        QVector<QPair<QDateTime, qreal>> data;
};

#endif // SENSOR_HPP
