#ifndef AGRAPH_HPP
# define AGRAPH_HPP

#  define FORMAT "yyyy-MM-dd hh:mm:ss"

#  include <QtCharts>
#  include <QVector>
#  include <QPair>

class AGraph {
    public:
        AGraph();
        virtual ~AGraph();

        enum e_step {
            HOUR = 0,
            HOURS = 1,
            DAY = 2
        };

        enum e_averaging {
            AVERAGE = 0,
            MAX = 1,
            MIN = 2
        };

        virtual void  exposeCharts(QDateTime from, QDateTime to,
                                   QVector<QPair<QDateTime, qreal>>) = 0;

        void buildGraph(QDateTime from, QDateTime to,
                        QVector<QPair<QDateTime, qreal>> data,
                        e_averaging);
        qreal getAverage(QVector<qreal>);
        QVector<QPair<QDateTime, qreal>> getDataWithStep(QVector<QPair<QDateTime, qreal>>, e_step);
        QVector<QPair<QDateTime, qreal>> getDataWithMaxVal(QVector<QPair<QDateTime, qreal>>);
        QVector<QPair<QDateTime, qreal>> getDataWithMinVal(QVector<QPair<QDateTime, qreal>>);

        QChart* getChart() {return chart;}

    protected:
        QChart*        chart;
        QValueAxis*    axisY;
};

#endif // AGRAPH_HPP
