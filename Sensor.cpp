#include "Sensor.hpp"

Sensor::Sensor(QString const& name)
    : name(name)
{}

Sensor::~Sensor()
{}

void Sensor::addData(QDateTime qdtime, qreal value) {
    data.push_back(QPair<QDateTime, qreal>(qdtime, value));
}

QString const& Sensor::getName() const
{
    return name;
}

QVector<QPair<QDateTime, qreal>> const& Sensor::getData() const
{
    return data;
}
