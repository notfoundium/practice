#ifndef SPLINEGRAPH_HPP
#define SPLINEGRAPH_HPP

# include "LineGraph.hpp"

class SplineGraph : public LineGraph {
    public:
        SplineGraph();
        ~SplineGraph();
};

#endif // SPLINEGRAPH_HPP
