#include <Device.hpp>

Device::Device()
{}

Device::~Device()
{}

QVector<QString> Device::csvToTokens(std::istream& str)
{
    QVector<QString>   result;
    std::string            line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream, cell, ';'))
        result.push_back(QString(cell.c_str()));
    if (!lineStream && cell.empty())
        result.push_back("");
    return result;
}


void Device::fromCSV(const std::string& csv_str)
{
    std::istringstream iss(csv_str);
    header = csvToTokens(iss);
    title = csvToTokens(iss);
    QVector<QString> current = csvToTokens(iss);

    name = header[1];
    for (QVector<QString>::size_type i = 1; i < title.size(); ++i)
        sensors.push_back(Sensor(title[i]));
    for (int i = 0; current.size() > 1; ++i)
    {
        QDateTime current_dt = QDateTime::fromString(current[0], FORMAT);
        for (QVector<QString>::size_type j = 0; j < current.size() - 1; ++j)
            sensors[j].addData(current_dt, current[j + 1].toDouble());
        current = csvToTokens(iss);
    }
    sensors.pop_back();
}

void Device::fromJSON(QString const& json_str, QString const& name) {
    QJsonDocument json = QJsonDocument::fromJson(json_str.toUtf8());
    QJsonObject json_obj = json.object();
    bool is_init = false;

    for (const auto& i : json_obj.keys())
    {
        QJsonValue value = json_obj.value(QString(i));
        QJsonObject item = value.toObject();
        QDateTime dt = QDateTime::fromString(item.value(QString("Date")).toString(), FORMAT);
        QString uName = item.value(QString("uName")).toString();

        if (uName == name.left(name.lastIndexOf(QChar(' ')))) {
            QJsonObject temp = item.value(QString("data")).toObject();
            if (!is_init) {
                for (int i = 0; i < temp.keys().size(); ++i)
                    sensors.push_back(Sensor(temp.keys()[i]));
                is_init = true;
            }
            for (int i = 0; i < sensors.size(); ++i)
                sensors[i].addData(dt, temp.value(sensors[i].getName()).toString().toDouble());
        }
    }
}

QString const& Device::getName() const {
    return name;
}

Sensor const& Device::getSensor(int idx) const
{
    return sensors[idx];
}

int Device::getSensorCount() const {
    return sensors.size();
}
