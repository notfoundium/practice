#ifndef MAINWINDOW_HPP
# define MAINWINDOW_HPP

#  include <QMainWindow>
#  include <QtCharts>

#  include "Device.hpp"
#  include "LineGraph.hpp"
#  include "SplineGraph.hpp"
#  include "BarGraph.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

        enum e_type {
            LINE = 0,
            SPLINE = 1,
            BAR = 2
        };

    private slots:
        void on_action_exit_triggered();
        void on_pushButton_Build_clicked();
        void on_action_linear_triggered();
        void on_action_bar_triggered();
        void on_action_spline_triggered();
        void on_action_open_csv_triggered();
        void on_spinBox_valueChanged(int arg1);
        void on_radioButton_clicked();
        void on_radioButton_2_clicked();
        void on_radioButton_3_clicked();
        void on_action_open_json_triggered();

    private:
        Ui::MainWindow *ui;

        AGraph::e_averaging averaging;

        e_type      type;
        Device*     device;
        AGraph*     graph;
        QByteArray* raw_json;
};

#endif // MAINWINDOW_HPP
