#include "BarGraph.hpp"

BarGraph::BarGraph()
    : series(new QBarSeries)
{
    axisX = new QBarCategoryAxis;
    chart->addAxis(axisX, Qt::AlignBottom);
}

BarGraph::~BarGraph()
{}

void BarGraph::exposeCharts(QDateTime from, QDateTime to, QVector<QPair<QDateTime, qreal>> data)
{
    qreal   max;
    bool    is_first = true;
    QBarSet *set = new QBarSet("time");
    QDateTime dt_min;
    QDateTime dt_max;

    for (QVector<QPair<QDateTime, qreal>>::size_type i = 0; i < data.size(); ++i) {
        QDateTime current = data[i].first;

        if (current < from)
            continue;
        if (current > to)
            break;
        if (is_first) {
            dt_max = data[i].first;
            dt_min = data[i].first;
            max = data[i].second;
            is_first = false;
        }
        else {
            dt_max = (data[i].first > dt_max) ? data[i].first : dt_max;
            dt_min = (data[i].first < dt_min) ? data[i].first : dt_min;
            max = (data[i].second > max) ? data[i].second : max;
        }
        *set << data[i].second;
    }
    series->append(set);
    chart->addSeries(series);
    axisX->setLabelsVisible(false);
    axisX->setTitleText(dt_min.toString() + " - " + dt_max.toString());
    axisY->setMax(max + max / 10);
    series->attachAxis(axisX);
    series->attachAxis(axisY);
}
