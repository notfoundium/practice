#ifndef LINEGRAPH_HPP
#define LINEGRAPH_HPP

# include "AGraph.hpp"

class LineGraph : public AGraph {
    public:
        LineGraph();
        virtual ~LineGraph();

        void exposeCharts(QDateTime from, QDateTime to, QVector<QPair<QDateTime, qreal>>);

    protected:
        QLineSeries*   series;
        QDateTimeAxis* axisX;
};

#endif // LINEGRAPH_HPP
