#ifndef BARGRAPH_HPP
# define BARGRAPH_HPP

#  include "AGraph.hpp"

class BarGraph : public AGraph {
    public:
        BarGraph();
        ~BarGraph();

        void  exposeCharts(QDateTime from, QDateTime to, QVector<QPair<QDateTime, qreal>>);

    private:
        QBarSeries*       series;
        QBarCategoryAxis* axisX;
};

#endif // BARGRAPH_HPP
