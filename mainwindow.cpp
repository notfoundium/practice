﻿#include "mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , averaging(AGraph::AVERAGE)
    , type(LINE)
    , device(nullptr)
    , graph(nullptr)
    , raw_json(nullptr)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    if (device != nullptr)
        delete device;
    delete ui;
}

void MainWindow::on_action_exit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_pushButton_Build_clicked()
{
    if (graph) {
        delete graph;
        graph = nullptr;
    }
    if (!device && !raw_json)
        return;

    if (type == LINE)
        graph = new LineGraph();
    if (type == SPLINE)
        graph = new SplineGraph();
    if (type == BAR)
        graph = new BarGraph();
    if (raw_json) {
        if (device != nullptr)
            delete device;
        device = new Device;
        device->fromJSON(*raw_json, ui->comboBox->currentText());

        ui->comboBox->setEnabled(true);
        if (device->getSensorCount() > 0)
        {
            ui->lineEdit_SensorName->setText(device->getSensor(ui->spinBox->value()).getName());
            ui->spinBox->setMaximum(device->getSensorCount() - 1);
            ui->graphicsView->setChart(graph->getChart());
            graph->buildGraph(ui->dateTimeEdit_From->dateTime(), ui->dateTimeEdit_To->dateTime(),
                              device->getSensor(ui->spinBox->value()).getData(),
                              averaging);
        }
        else
        {
            delete device;
            device = nullptr;
        }
    }
    else
    {
        ui->graphicsView->setChart(graph->getChart());
        graph->buildGraph(ui->dateTimeEdit_From->dateTime(), ui->dateTimeEdit_To->dateTime(),
                          device->getSensor(ui->spinBox->value()).getData(),
                          averaging);
    }
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
}

void MainWindow::on_action_open_json_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("JSON (*.json)"));
    if (raw_json) {
        delete raw_json;
        raw_json = nullptr;
    }
    raw_json = new QByteArray;

    ui->spinBox->setValue(0);
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    *raw_json = file.readAll();
    if (device != nullptr)
        delete device;
    device = new Device;
    device->fromJSON(*raw_json, ui->comboBox->currentText());

    ui->comboBox->setEnabled(true);
    if (device->getSensorCount() > 0)
    {
        ui->lineEdit_SensorName->setText(device->getSensor(ui->spinBox->value()).getName());
        ui->spinBox->setMaximum(device->getSensorCount() - 1);
    }
    else
    {
        delete device;
        device = nullptr;
    }
}

void MainWindow::on_action_open_csv_triggered()
{
    QString     fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("CSV (*.csv)"));
    QTextCodec* codec = QTextCodec::codecForName("Windows-1251");
    QByteArray  val;

    if (raw_json) {
        delete raw_json;
        raw_json = nullptr;
    }
    ui->spinBox->setValue(0);
    if (fileName.isEmpty())
        return;
    QFile file(fileName);

    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    if (device != nullptr)
        delete device;
    device = new Device;
    device->fromCSV(codec->toUnicode(val).toStdString());
    if (device->getSensorCount() > 0) {
        ui->comboBox->setCurrentText(device->getName());
        ui->comboBox->setEnabled(false);
        ui->lineEdit_SensorName->setText(device->getSensor(ui->spinBox->value()).getName());
        ui->spinBox->setMaximum(device->getSensorCount() - 1);
    }
    else {
        delete device;
        device = nullptr;
    }
}


void MainWindow::on_spinBox_valueChanged(int arg1)
{
    if (device)
        ui->lineEdit_SensorName->setText(device->getSensor(arg1).getName());
}

void MainWindow::on_action_linear_triggered()
{
    type = LINE;
}

void MainWindow::on_action_bar_triggered()
{
    type = BAR;
}

void MainWindow::on_action_spline_triggered()
{
    type = SPLINE;
}

void MainWindow::on_radioButton_clicked()
{
    averaging = AGraph::AVERAGE;
}

void MainWindow::on_radioButton_2_clicked()
{
    averaging = AGraph::MAX;
}

void MainWindow::on_radioButton_3_clicked()
{
    averaging = AGraph::MIN;
}

