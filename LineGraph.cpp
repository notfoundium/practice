#include "LineGraph.hpp"

LineGraph::LineGraph()
    : axisX(new QDateTimeAxis)
{
    series = new QLineSeries;
}

void LineGraph::exposeCharts(QDateTime from, QDateTime to,
                             QVector<QPair<QDateTime, qreal>> data) {
    bool is_first = true;
    qreal max;
    qreal min;

    for (QVector<QPair<QDateTime, qreal>>::size_type i = 0; i < data.size(); ++i)
    {
        QDateTime current = data[i].first;

        if (current < from)
            continue;
        if (current > to)
            break;
        if (is_first) {
            max = data[i].second;
            min = data[i].second;
            is_first = false;
        }
        else {
            max = (data[i].second > max) ? data[i].second : max;
            min = (data[i].second < min) ? data[i].second : min;
        }
        series->append(data[i].first.toMSecsSinceEpoch(), data[i].second);
    }
    chart->addSeries(series);
    axisY->setRange(min - min / 10, max + max / 10);
    axisX->setRange(from, to);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    series->attachAxis(axisY);
}

LineGraph::~LineGraph()
{}
