#ifndef DEVICE_HPP
# define DEVICE_HPP

#  define FORMAT "yyyy-MM-dd hh:mm:ss"

#  include <sstream>

#  include <QString>
#  include <QVector>
#  include <QJsonDocument>
#  include <QJsonObject>

#  include <QDebug>

#  include "Sensor.hpp"

class Device {
    public:
        Device();
        ~Device();

        void fromJSON(QString const& json_str, QString const& name);
        void fromCSV(std::string const& csv_str);

        QVector<QString> csvToTokens(std::istream& str);

        QString const& getName() const;
        Sensor const&  getSensor(int idx) const;
        int            getSensorCount() const;

    private:
        QString          name;
        QVector<QString> header;
        QVector<QString> title;
        QVector<Sensor>  sensors;
};

#endif // DEVICE_HPP
